## Designed for changing repositories in the CachyOS LiveCD environment.

A stripped down, possibly temporary version. Based on [script](https://mirror.cachyos.org/cachyos-repo.tar.xz), [repo](https://github.com/CachyOS/linux-cachyos#how-to-add-cachyos-repositories).