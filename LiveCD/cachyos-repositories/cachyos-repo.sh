#!/bin/bash
# A stripped down, possibly temporary version. Based on https://mirror.cachyos.org/cachyos-repo.tar.xz (https://github.com/CachyOS/linux-cachyos#how-to-add-cachyos-repositories)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

info() {
    local mesg=$1; shift
    printf "${YELLOW} -->${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "$@" >&2
}

check_supported_isa_level() {
    /lib/ld-linux-x86-64.so.2 --help | grep "$1 (supported, searched)" > /dev/null
    echo $?
}

check_if_repo_was_added() {
    cat /etc/pacman.conf | grep "(cachyos-v3\|cachyos-core-v3\|cachyos-extra-v3\|cachyos-testing-v3\|cachyos-v4)" > /dev/null
    echo $?
}

add_specific_repo() {
    local isa_level="$1"
    local gawk_script="$2"
    local repo_name="$3"
    local cmd_check="check_supported_isa_level ${isa_level}"

    local pacman_conf="/etc/pacman.conf"
    local pacman_conf_cachyos="./pacman.conf"
    local pacman_conf_path_backup="/etc/pacman.conf.bak"

    local is_isa_supported="$(eval ${cmd_check})"
    if [ $is_isa_supported -eq 0 ]; then
        info "${isa_level} is supported"

        cp $pacman_conf $pacman_conf_cachyos
        gawk -i inplace -f $gawk_script $pacman_conf_cachyos || true

        info "Backup old config"
        mv $pacman_conf $pacman_conf_path_backup

        info "CachyOS ${repo_name} Repo changed"
        mv $pacman_conf_cachyos $pacman_conf
    else
        info "${isa_level} is not supported"
    fi
}

run() {
local is_repo_added="$(check_if_repo_was_added)"

if [ $is_repo_added -ne 0 ]; then
        add_specific_repo x86-64-v3 ./install-repo.awk cachyos-v3
        add_specific_repo x86-64-v4 ./install-v4-repo.awk cachyos-v4
    else
        info "Repo is already added!"
fi
}

run